#!/usr/bin/env bash

# This is a script to download the OpenWRT firmware for a GL-MT300A router
#
# Copyright (c) 2016 Chris Croome
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# The OpenWRT firmware
URL="https://downloads.openwrt.org/snapshots/trunk/ramips/mt7620/openwrt-ramips-mt7620-gl-mt300a-squashfs-sysupgrade.bin"
# The file it is to be saved to
FILE="openwrt.bin"

# Check which is available 
WHICH=$(which which)
if [[ -z "${WHICH}" ]]; then
  echo "Sorry which couldn't be found, please install it"
  exit 1
fi

# The current working directory 
PWD=$(${WHICH} pwd)
if [[ -z "${PWD}" ]]; then
  echo "Sorry pwd couldn't be found, please install it"
  exit 1
else
  DIR=$(pwd)
fi

# On OSX md5sum is md5, check which we have 
MD5SUM=$(${WHICH} md5sum)
# If we don't have md5sum check if we have md5 (OSX)
if [[ -z "${MD5SUM}" ]]; then
  MD5SUM=$(${WHICH} md5)
fi

# Check we have wget or curl available 
WGET=$(${WHICH} wget)
if [[ -z "${WGET}" ]]; then
  CURL=$(${WHICH} curl)
  if [[ -e "${CURL}" ]]; then
    GETFILE="${CURL} -s -o ${DIR}/${FILE} ${URL}"
  else
    echo "You need to install wget or curl!"
    exit 1
  fi
else 
  GETFILE="${WGET} -q -O ${DIR}/${FILE} ${URL}"
fi

# Download the file
${GETFILE} && echo "Firmware downloaded to ${DIR}/${FILE}" \
  || { echo "There was a problem downloading ${URL}, check you internet connection?" ; exit 1 ; }
# generate a md5sum, this is needed as a check when flashing
if [[ -e "${MD5SUM}" ]]; then
  MD5=$(${MD5SUM} ${DIR}/${FILE})
  echo "${MD5}"
  echo "The md5sum above can be used to check that the upload of the firmware has gone OK"
fi

exit 0
