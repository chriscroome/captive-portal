#!/usr/bin/env bash

# This is a script to set up Nginx and Dnsmasq on a GL-MT300A router
#
# Copyright (c) 2016 Chris Croome
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# The IP addess of the device, by default this is 192.168.1.1
IP="192.168.1.1"

# Mount point for the SD Card
MNT="/www/html"

WHICH=$(which which)
if [[ -z "${WHICH}" ]]; then
  echo "Sorry which couldn't be found, please install it"
  exit 1 
fi

# Check ssh and scp are available 
SSHCHECK=$(${WHICH} ssh)
SCPCHECK=$(${WHICH} scp)
if [[ -e "${SSHCHECK}" && -e "${SCPCHECK}" ]]; then
  # The SSH command, using the default user and not checking the host key
  SSH="${SSHCHECK} -q -oStrictHostKeyChecking=no root@${IP}"
  # The SCP command, not checking the host key
  SCP="${SCPCHECK} -q -oStrictHostKeyChecking=no"
else
  echo "This script depends on ssh and scp, please install them"
  exit 1
fi

# The current working directory 
PWD=$(${WHICH} pwd)
if [[ -z "${PWD}" ]]; then
  echo "Sorry pwd couldn't be found, please install it"
  exit 1
else
  DIR=$(pwd)
fi

# Update the package list
echo "Updating the package list..."
${SSH} "opkg -V0 update" \
  && echo "Package list updated" \
  || { echo "There was a problem updating the package list on the router" ; exit 1 ; }

# Install and enable Nginx and Dnsmasq
if [[ -f "${DIR}/nginx.conf" && -f "${DIR}/dnsmasq.conf" ]]; then
  echo "Installing  and configuring Nginx and Dnsmasq"
  ${SSH} "opkg -V0 install nginx dnsmasq" && echo "Nginx and Dnsmasq installed" \
    || { echo "Problem installing Nginx and Dnsmasq" ; exit 1 ; }
  # Copy the Nginx config to the router
  ${SCP} ${DIR}/nginx.conf root@${IP}:/etc/nginx/ && echo "${DIR}/ngnix.conf copied to the router" \
    || { echo "Problem copying ngnix.conf to the router" ; exit 1 ; }
  ${SSH} "/etc/init.d/nginx enable ; /etc/init.d/nginx reload" \
    && echo "Nginx enabled and reloaded" || { echo "Problem enabling and reloading Nginx" ; exit 1 ; }
  # Copy the Dnsmasq config to the router
  ${SCP} ${DIR}/dnsmasq.conf root@${IP}:/etc/ && echo "${DIR}/dnsmasq.conf copied to the router" \
    || { echo "Problem copying dnsmasq.conf to the router" ; exit 1 ; }
  ${SSH} "/etc/init.d/dnsmasq enable ; sleep 2 ; /etc/init.d/dnsmasq restart" \
    && echo "Dnsmasq enabled and reloaded" || { echo "Problem enabling and reloading Dnsmasq" ; exit 1 ; }
else
  echo "No nginx.conf and / dnsmasq.conf file(s) found in ${DIR} so Nginx and Dnsmasq are not being installed and configured"
fi 

echo "Nginx and Dnsmasq installed and configured, test by visting http://192.168.1.1/ and / or http://example.test/" 

exit 0
