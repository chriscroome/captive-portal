#!/usr/bin/env bash

# This is a script to format, a SD Card on a GL-MT300A router. 
#
# Copyright (c) 2016 Chris Croome
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details. 
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# The IP addess of the device, by default this is 192.168.1.1
IP="192.168.1.1"

# The block device on the router
DEV="/dev/mmcblk0"

# The first partition on the SD Card
P1="/dev/mmcblk0p1"

# Mount point for the SD Card on the router
MNT="/www/html"

# Check which is available 
WHICH=$(which which)
if [[ -z "${WHICH}" ]]; then
  echo "Sorry which couldn't be found, please install it"
  exit 1
fi

# Check ssh and scp are available 
SSHCHECK=$(${WHICH} ssh)
SCPCHECK=$(${WHICH} scp)
if [[ -e "${SSHCHECK}" && -e "${SCPCHECK}" ]]; then
  # The SSH command, using the default user and not checking the host key
  SSH="${SSHCHECK} -q -oStrictHostKeyChecking=no root@${IP}"
  # The SCP command, not checking the host key
  SCP="${SCPCHECK} -q -oStrictHostKeyChecking=no"
else
  echo "This script depends on ssh and scp, please install them"
  exit 1
fi

# The current working directory 
PWD=$(${WHICH} pwd)
if [[ -z "${PWD}" ]]; then
  echo "Sorry pwd couldn't be found, please install it"
  exit 1
else
  DIR=$(pwd)
fi

# Is the SD Card already mounted?
MNTCHECK=$(${SSH} "cat /proc/mounts | grep ${MNT}")
if [[ "${MNTCHECK}" ]]; then
  echo "The SD Card at ${P1} appears to already been mounted at ${MNT}" 
  exit
fi

# Update the package list
echo "Updating the package list..."
${SSH} "opkg -V0 update" \
  && echo "Package list updated" \
  || { echo "There was a problem updating the package list on the router" ; exit 1 ; }

# Install kerenel modules and filesystem tools
echo "Installing kernel modules and packages, this will take a little while..."
# The following command always returns errors with "sdhci-pltfm is already loaded" 
# ignore this issue and and everything is fine, see the list of optional kernel modules:
# https://wiki.openwrt.org/toh/gl-inet/gl-mt300a#basic_configuration 
${SSH} "opkg -V0 install kmod-sdhci-mt7620 &>/dev/null"
# The following work-around was based on this one 
# https://dev.openwrt.org/ticket/20993#comment:1
${SSH} "sed -i -e 's/sdhci-pltfm/#sdhci-pltfm/' /usr/lib/opkg/info/kmod-sdhci.postinst-pkg"
# Installing block-mount generated the following error which appears to be safe to ignore
# //usr/lib/opkg/info/block-mount.postinst: .: line 130: can't open './10-fstab'
${SSH} "opkg -V0 install block-mount &>/dev/null" 
# Install USB and FS kernel modules and files system tools
${SSH} "opkg -V0 install kmod-rt2800-usb kmod-usb-storage kmod-fs-ext4 fdisk block-mount e2fsprogs" \
  && echo "Kernel modules and filesystem tools installed" \
  || { echo "Problem installing kernel modules and filesystem tools" ; exit 1 ; }

# Check that we have a SD Card
SDCHECK=$(${SSH} "if [[ -b ${DEV} ]]; then echo 'SD Card found' ; else 'No SD Card found' ; fi") 
if [[ "${SDCHECK}" = "SD Card found" ]]; then
  # Upload fdisk script 
  if [[ -f "${DIR}/fdisk.sh" ]]; then
    ${SCP} "${DIR}/fdisk.sh" root@${IP}:/tmp/ || { echo "Problem uploading ${DIR}/fdisk.sh" ; exit 1 ; }  
    ${SSH} "chmod 755 /tmp/fdisk.sh" || { echo "Problem making /tmp/fdisk.sh executable" ; exit 1 ; } 
  else
    echo "${DIR}/fdisk.sh not found"
    exit 1
  fi
  # Run the fdisk.sh script
  ${SSH} "/bin/sh /tmp/fdisk.sh &>/dev/null" && echo "SD Card formatted" \
    || { echo "Problem formatting the SD Card" ; exit 1 ; }
  # Create a ext4 filesystem, use force to prevent a prompt and do it quietly
  ${SSH} "mkfs.ext4 -F -q ${P1}" && echo "ext4 filesystem created on the SD Card" \
    || { echo "Problem creating a filesystem on the SD Card" ; exit 1 ; }
  # Make the mount point and mount the SD Card
  MNTEXISTS=$(${SSH} "if [[ -d ${MNT} ]]; then echo 1; fi")
  if [[ "${MNTEXISTS}" != "1" ]]; then
    ${SSH} "mkdir -p ${MNT}" && echo "Mount point at ${MNT} created" \
      || { echo "Problem creating a mount point at ${MNT}" ; exit 1 ; } 
  fi
  # The following is suggested on the wiki but doesn't quite work 
  # https://wiki.openwrt.org/doc/howto/storage
  #${SSH} "/etc/init.d/fstab stop"
  #${SSH} "uci add fstab mount"
  #${SSH} "uci set fstab.@mount[-1].device=/dev/mmcblk0p1"
  #${SSH} "uci set fstab.@mount[-1].options=rw,sync"
  #${SSH} "uci set fstab.@mount[-1].enabled_fsck=0"
  #${SSH} "uci set fstab.@mount[-1].enabled=1"
  #${SSH} "uci set fstab.@mount[-1].target=/www/html"
  #${SSH} "uci commit fstab"
  #${SSH} "/etc/init.d/fstab start"
  # Because the above doesn't work exactly, we have to edit fstab manually, first get a tab character 
  # https://lists.debian.org/debian-user/2003/05/msg05269.html
  TAB=$(printf "\t")
  # Then use that when editing the /etc/config/fstab file
  ${SSH} "sed -i -e \"s/option[[:space:]]enabled[[:space:]]'0'/option${TAB}enabled${TAB}${TAB}'1'/\" /etc/config/fstab"
  # Then delete the trailing line at the end 
  # https://stackoverflow.com/a/4881990
  ${SSH} "sed -i '$ d' /etc/config/fstab"
  # Then add some lines at the end of the file
  ${SSH} "echo -e \"${TAB}option${TAB}device${TAB}${TAB}'/dev/mmcblk0p1'\" >> /etc/config/fstab"
  ${SSH} "echo -e \"${TAB}option${TAB}options${TAB}${TAB}'rw,sync'\" >> /etc/config/fstab"
  ${SSH} "echo -e \"${TAB}option${TAB}enabled_fsck${TAB}'0'\" >> /etc/config/fstab"
  ${SSH} "echo -e \"${TAB}option${TAB}target${TAB}${TAB}'/www/html'\" >> /etc/config/fstab"
  ${SSH} "echo -e \"${TAB}option${TAB}fstype${TAB}${TAB}'ext4'\" >> /etc/config/fstab"
  ${SSH} "mount -t ext4 ${P1} ${MNT}" && echo "SD Card mounted at ${MNT}" \
    || { echo "Problem mounting the SD Card at ${MNT}" ; exit 1 ; }
else
  echo "Please check that a SD Card is installed, if it is it should result in a block device"
  echo "appearing at ${DEV}"
fi

exit 0
