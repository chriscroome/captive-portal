# OpenWRT Captive Portal WIFI Web Server

These scripts can be used to configure a [GL-Inet GL-MT300A](https://www.gl-inet.com/mt300a/), 
running [OpenWRT](https://wiki.openwrt.org/toh/gl-inet/gl-mt300a) as a captive 
portal with a web server running on port 80, these routers are 
[available from amazon.co.uk](https://www.amazon.co.uk/dp/B01DBS5Z0W/) and the scripts are 
[available at GitLab](https://gitlab.com/chriscroome/captive-portal).

| Photos                                                 |                                                                     |                                                     |
| ------------------------------------------------------ | ------------------------------------------------------------------- | --------------------------------------------------- |
| ![USB battery powering the router](captive-portal.png) | ![Opening the case](opening-the-case.png)                           | ![Inserting the SD Card](insert-the-sd-card.png)    |   
| ![Replacing the base](replacing-the-base.png)          | ![USB to UART for Serial Access](usb-to-uart-for-serial-access.png) | ![Serial Console Output](serial-console-output.png) |

__Table of Contents__

1. [Download Files](#download-files)
2. [Install a Micro SD Card](#install-a-micro-sd-card)
3. [Update the Firmware](#update-the-firmware) 
4. [Connect to and Secure the Router](#connect-to-and-secure-the-router)
5. [Format and Mount the SD Card](#format-and-mount-the-sd-card)
6. [Upload a Web Site to the Router](#upload-a-web-site-to-the-router)
7. [Install a Web Server](#install-a-web-server)
8. [Access the Web Site on the Router](#access-the-web-site-on-the-router)
9. [Configure Multiple Routers](#configure-multiple-routers)
10. [Troubleshooting](#troubleshooting)
    1. [OSX Network Settings](#osx-network-settings)
    2. [Debian Network Settings](#debian-network-settings)
    3. [Serial Console](#serial-console)

## Download Files

Before you connect the router to your computer download the files in this repository: 

```bash
git clone https://gitlab.com/chriscroome/captive-portal.git
```

Then change into the directory that was created and run the `openwrt.sh` script to download 
[the latest trunk version of OpenWRT](https://downloads.openwrt.org/snapshots/trunk/ramips/mt7620/openwrt-ramips-mt7620-gl-mt300a-squashfs-sysupgrade.bin), 
which will be saved as `openwrt.bin` in the current working directory: 

```bash
cd captive-portal
./openwrt.sh
```

This script will also generate an md5sum of `openwrt.bin`.

## Install a Micro SD Card 

Install a micro SD Card into the router, see 
[this YouTube video](https://www.gl-inet.com/how-to-open-the-case-of-gl-mini-routers/) 
and the 
[photos and notes on the OpenWRT wiki](https://wiki.openwrt.org/toh/gl-inet/gl-mt300a#opening_the_case)
for instructions on how to open the case. 

If you have a USB to UART cable, see the [serial console notes](#serial-console) 
below, then also connect this and don't put the router back into it's case, if
you don't have one of these leads then the router can be put back into it's 
case.

## Update the Firmware 

Power the router using a decent power supply (5V 1A) and USB to micro USB cable
via the Ethernet port on your computer (if you don't have one you will need to
get an adaptor) to the LAN port on the router (you don't need to connect the
WAN port at this stage).

If you have connected a serial console cable then use `screen` to connect to it
following the [serial console notes](#serial-console). 

Use a web browser to access the router using HTTP via http://192.168.8.1/ then
_"Choose Your Language"_, _"Choose Your Timezone"_, _"Set Your Password"_, then
click _"Advanced settings"_ (in the top right hand corner of the page) and
login as root, using the password you just set. 

Go to _"System -> Backup / Flash Firmware"_ and use the form under _"Flash new
firmware image"_ untick _Keep settings_ and _Browse_ to select `openwrt.bin`
and click _Flash image_, you are then presented with the md5sum of
`openwrt.bin`, check this against the one generated when you ran `openwrt.sh`
and then click _"Proceed"_. Flashing the firmware usually takes under two
minutes, don't power it down while it is updating. If you have connected a
serial console cable to the router then you are able to see what is happening
via the console.

If the update goes wrong and the router doesn't respond then, try waiting a
little while before you check [the troubleshooting suggestions
below](#troubleshooting) and if all else fails manually set your Ethernet port
to 192.168.1.2, gateway 192.168.1.1, netmask 255.255.255.0 and then power down
down the router by unplugging it, hold down the reset button, power it on and
wait until the red LED flashes three times and then flickers, then let go of
the reset button and in a web browser go to http://192.168.1.1/ and upload a
new firmware package. Ensure that you have the Ethernet connection on your
computer up before you boot into the firmware upgrade mode.

## Connect to and Secure the Router

By default OpenWRT doesn't have WIFI or a web interface enabled and uses a
different default subnet from the firmware the router comes with, once you have
flashed the OpenWRT firmware the routers IP address will be `192.168.1.1`.

Leave the Ethernet cable connecting your computer to the LAN port in place and
now also and connect the WAN port to your network via Ethernet.

The router will be running a DHCP server so you shouldn't need to manually
configure your Ethernet connection but you might need to restart it (see the
[OSX](#osx-network-settings) and [Debian](#debian-network-settings) networking
hints below). 

If you can't get it working with DHCP then manually set your computers Ethernet
interface up then use 192.168.1.2  for your computer and set the gateway to
192.168.1.1 and netmask to 255.255.255.0.

You won't be able to access the router via http://192.168.1.1/ because it won't
be running a web server, `ssh` to the router to check that all everything is
OK, initially there is no root password set:

```bash
ssh root@192.168.1.1
```

If you get _"Connection refused"_ then try waiting 30 seconds or so and trying
again -- it takes a little while for all the services to be brought up. 

If you get _"WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!"_ then this is
because the last computer at 192.168.1.1 that you connected to had a different
SSH key -- this is to be expected -- run the following and try again:

```bash
ssh-keygen -f ~/.ssh/known_hosts -R 192.168.1.1
```

The next thing to do is to run the `passwd.sh` script which uploads a
`authorized_keys` file from the current working directory (if it exists) and
then sets a root password on the router.

The next thigs to do is to upload your ssh public key(s) to the router and set
the root password.

It is essential to upload a `authorized_keys` conteing your OpenSSH public keys
as you will keep being prompted to enter the password if you don't, if you
don't have a ssh keypair you can generate one by running the following OpenSSH
command (if you set a passphrase on the private key then you probably also want
to use a ssh agent to store the passphrase to save typing it):

```bash
ssh-keygen -t rsa -b 4096
```

If you want you can set the password in an environmental variable,
`$OPENWRTPASSWD`, before you run the script and also copy your public ssh
key(s) to the current working directory before running the `passwd.sh` script:

```bash
export OPENWRTPASSWD="foobarbaz"
cat ~/.ssh/*.pub > authorized_keys
./passwd.sh
```

The `passwd.sh` script also enables WIFI on the router, after it has been run
you can connect to it via SSID OpenWRT, note that it is unencrypted and no
passphrase is needed, if you want to change this see [the OpenWRT
wiki](https://wiki.openwrt.org/doc/uci/wireless#wifi_networks), you will need
to manually edit `/etc/config/wireless` or use `uci` to set it up, the `wifi`
command can be used to reload the settings and `wifi down` to turn it off
again.

## Format and Mount the SD Card

The `sdcard.sh` script format the SD card (it assumes there is an existing
partition on the SD card and deletes this using the `fdisk.sh` script which it
ulpoad to the router and then creates an ext4 filesystem) and mounts it at
`/www/html`:

```bash
./sdcard.sh
```

## Upload a Web Site to the Router

Put the website you wish to upload into a sub-directory named `html`, or run
the script without doing that and a simple `index.html` file will be generated
for you, run `rsync.sh` to upload the website:

```bash
./rsync.sh
```

If you want to update the website on the router you can simply run `./rsync.sh`
again, this will copy the files from the `html` sub-directory tot he router.

## Install a Web Server

The `nginx.sh` script installs and configures Nginx (using the basic
`nginx.conf` configuration file in this repository) and Dnsmasq. Note that
Dnsmasq will be configured to resolve all DNS requests to the localhost so if
you are connecting to the internet through the router you might need to use IP
addresses to ssh to servers and a ssh tunnel for your web browser, or simply
disconnect the router from your Ethernet port after this last step and test it
using WIFI (you might need to disable the mobile phone network if testing via a
phone, turn on "airplane mode" and then enable WIFI):

```bash
./nginx.sh
```

## Access the Web Site on the Router

Disconnect the Ethernet cables and reboot the router and you should now be able
to connect to the router via WIFI, SSID **OpenWRT**, then in a web browser go
to any website (on port 80, port 443 can't be used), for example
http://test.test/ (or simply use the IP address, http://192.168.1.1/) and you
should see the files you uploaded from the `html` subdirectory.

## Configure Multiple Routers

Once [the new firmware has been loaded onto the router](#update-the-firmware)
all the above commands can be set off to run after each other if you have
multiple routers to configure, you will need to: 

```bash
./passwd.sh  && { ./sdcard.sh ; ./rsync.sh ; ./nginx.sh ; } || { sleep 10 ; echo "Please wait a moment and try again, sshd might not have started" ; }
```

## Troubleshooting

When you are flashing the firmware using the web interface you need to manually
setup your network as there isn't a DHCP server running on the router, in
addition your network needs to be configured before the router is booted,
following are some notes on manually configuring network settings on OSX and
Debian.

The best way to debug issues with the router is through having access to a
serial console as this gives you a shell on the device that still works when
the router has no network access, see below for the hardware and software
needed to set up [serial console access](#serial-console). 

### OSX Network Settings

If you are running OSX you will need to manually set the network settings for
you Ethernet interface, see the following screenshot for an example of how to
do this.

![OSX Network Settings](osx-network-settings.png)

### Debian Network Settings

The following notes are based on the assumption that you are using a
[Debian](https://debian.org/) machine but they should be more-or-less the same
for [Ubuntu](https://www.ubuntu.com/) and other Debian based GNU/Linux
distribution.

Restart the network on `eth0` as `root`, this can be used when the network
changes after the firmware is flashed:

```bash
ifdown eth0 ; sleep 5 ; ifup eth0
```

If you have a problem connecting to the router try pinging the router:

```bash
ping 192.168.1.1
```

Try a portscan, if only port 80 is available it is probably waiting for you to
apply a firmware update, if only port 22 is open try ssh'ing to it:

```bash
nmap 192.168.1.1
```
If the above didn't help, check your network interfaces:

```bash
/sbin/ifconfig
```

And check your routing table:

```bash
/sbin/route -n 
```

And delete and add a default route:

```bash
route del default
route add default gw 192.168.1.1

```

If you need to then set up `eth0` manually:

```bash
/sbin/ifconfig eth0 192.168.1.2
/sbin/route add -net 192.168.1.0 netmask 255.255.255.0 gw 192.168.1.1 dev eth0
```

Or on Debian edit `/etc/network/interfaces` to manually configure `eth0`:

```
auto eth0
# iface eth0 inet dhcp
iface eth0 inet static
 address 192.168.1.2
 gateway 192.168.1.1
 netmask 255.255.255.0
```

Then run `ifdown eth0` and `ifup eth0` to bring the interface up and down.

Check you don't have a proxy set in your web browser or your environment:

```bash
printenv | grep -i proxy
```

Try a text mode browser if Firefox can't connect:

```bash
lynx http://192.168.1.1/
```

### Serial Console

Get a [CP2104 Serial Converter USB 2.0 To TTL UART 6PIN Module from Amazon](https://www.amazon.co.uk/gp/product/B01CYBHM26/), 
which is [documented in this PDF](https://www.silabs.com/Support%20Documents/TechnicalDocs/CP2104.pdf), 
or something like it and connect it 
[as documented on the OpenWRT wiki](https://wiki.openwrt.org/toh/gl-inet/gl-mt300a#serial) 
and [on the GL-Inet site](https://www.gl-inet.com/docs/diy/serial/) (this documents how to 
make the physical connection and the use of kermit on Linux / OSX and PuTTY on Windows).

If you are using OSX then you need to 
[install a driver from Silcon Labs](https://www.silabs.com/products/mcu/pages/usbtouartbridgevcpdrivers.aspx#mac)
and the device will appear as `/dev/cu.SLAB_USBtoUART`.

The __TX__, __RX__ and __GND__ pins are marked on both boards, plug leads into
TX, RX and GND and swap the TX and RX at the other end -- TX to RX and RX to
TX, connect the USB and it should register in `/var/log/messages` where you can
check what tty has been allocated, probably `/dev/tty/USB0`.

Use `screen` as [suggested on the OpenWRT wiki](https://wiki.openwrt.org/doc/hardware/port.serial#terminal_software) 
and
as `root` or using `sudo` on Linux:

```bash
screen /dev/ttyUSB0 115200
```

Or on OSX:

```bash
screen /dev/cu.SLAB_USBtoUART 115200
```

You should then see the serial console output, note that there is no password
needed to gain root access (just hit enter) and as the router boots you should
get output like this in screen:


```
U-Boot 1.1.3 (Dec 23 2015 - 18:37:46)

Board: Ralink APSoC DRAM:  128 MB
relocate_code Pointer2 at: 87fac000
enable ephy clock...done. rf reg 29 = 5
SSC disabled.
spi_wait_nsec: 29
spi device id: ef 40 18 0 0 (40180000)
find flash: W25Q128FV
*** Warning - bad CRC, using default environment

======================================================
uboot-gl-mt7620 version: 0.0.0.1
--------------------------------------------------
ASIC 7620_MP (Port5<->None)
DRAM component: 1024 Mbits DDR, width 16
DRAM bus: 16 bit
Total memory: 128 MBytes
Flash component: SPI Flash
Date:Dec 23 2015  Time:18:37:46
--------------------------------------------------
icache: sets:512, ways:4, linesz:32 ,total:65536
dcache: sets:256, ways:4, linesz:32 ,total:32768
--------------------------------------------------
 ##### The CPU freq = 580 MHZ ####
 ##### Memory size =128 Mbytes ####
======================================================

Press press WPS button for more than 2 seconds to run web failsafe mode

WPS button is pressed for:  0 second(s)

Catution: WPS button wasn't pressed or not long enough!
Continuing normal boot...


   *** ***         ** ** *****  ***   ***   ***    *
  *  *  *          ** ** * * * *   * *   * *   *   *
 *      *          ** **   *       * *   * *   *   **
 *      *    ***** ** **   *     **  *   * *   *  * *
 *  *** *          * * *   *       * *   * *   *  * *
 *   *  *          * * *   *       * *   * *   *  ****
  *  *  *   *      * * *   *   *   * *   * *   *  *  *
   **  ******      * * *  ***   ***   ***   ***  **  **


Hit SPACE to stop autoboot:  0
Found calibration data, checking calibration status...
Device calibrated. Checking MAC address...
Found MAC. Starting firmware...
## Booting image at bc050000 ...
   Image Name:   MIPS OpenWrt Linux-4.4.14
   Image Type:   MIPS Linux Kernel Image (lzma compressed)
   Data Size:    1334384 Bytes =  1.3 MB
   Load Address: 80000000
   Entry Point:  80000000
   Verifying Checksum ... OK
   Uncompressing Kernel Image ... OK
No initrd
## Transferring control to Linux (at address 80000000) ...
## Giving linux memsize in MB, 128

Starting kernel ...

[    0.000000] Linux version 4.4.14 (buildbot@owrtbuild01) (gcc version 5.3.0 (OpenWrt GCC 5.3.0 50013) ) #1 Sat Nov 26 16:37:08 UTC 2016
[    0.000000] Board has DDR2
[    0.000000] Analog PMU set to hw control
[    0.000000] Digital PMU set to hw control
[    0.000000] SoC Type: MediaTek MT7620A ver:2 eco:6
[    0.000000] bootconsole [early0] enabled
[    0.000000] CPU0 revision is: 00019650 (MIPS 24KEc)
[    0.000000] MIPS: machine is GL-MT300A
[    0.000000] Determined physical RAM map:
[    0.000000]  memory: 08000000 @ 00000000 (usable)
[    0.000000] Initrd not found or empty - disabling initrd
[    0.000000] Zone ranges:
[    0.000000]   Normal   [mem 0x0000000000000000-0x0000000007ffffff]
[    0.000000] Movable zone start for each node
[    0.000000] Early memory node ranges
[    0.000000]   node   0: [mem 0x0000000000000000-0x0000000007ffffff]
[    0.000000] Initmem setup node 0 [mem 0x0000000000000000-0x0000000007ffffff]
[    0.000000] Primary instruction cache 64kB, VIPT, 4-way, linesize 32 bytes.
[    0.000000] Primary data cache 32kB, 4-way, PIPT, no aliases, linesize 32 bytes
[    0.000000] Built 1 zonelists in Zone order, mobility grouping on.  Total pages: 32512
[    0.000000] Kernel command line: console=ttyS0,115200 rootfstype=squashfs,jffs2
[    0.000000] PID hash table entries: 512 (order: -1, 2048 bytes)
[    0.000000] Dentry cache hash table entries: 16384 (order: 4, 65536 bytes)
[    0.000000] Inode-cache hash table entries: 8192 (order: 3, 32768 bytes)
[    0.000000] Writing ErrCtl register=0007211b
[    0.000000] Readback ErrCtl register=0007211b
[    0.000000] Memory: 125548K/131072K available (3028K kernel code, 140K rwdata, 724K rodata, 196K init, 200K bss, 5524K reserved, 0K cma-reserved)
[    0.000000] SLUB: HWalign=32, Order=0-3, MinObjects=0, CPUs=1, Nodes=1
[    0.000000] NR_IRQS:256
[    0.000000] CPU Clock: 580MHz
[    0.000000] clocksource: systick: mask: 0xffff max_cycles: 0xffff, max_idle_ns: 583261500 ns
[    0.000000] systick: enable autosleep mode
[    0.000000] systick: running - mult: 214748, shift: 32
[    0.000000] clocksource: MIPS: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 6590553264 ns
[    0.000011] sched_clock: 32 bits at 290MHz, resolution 3ns, wraps every 7405115902ns
[    0.007575] Calibrating delay loop... 385.84 BogoMIPS (lpj=1929216)
[    0.070075] pid_max: default: 32768 minimum: 301
[    0.074703] Mount-cache hash table entries: 1024 (order: 0, 4096 bytes)
[    0.081073] Mountpoint-cache hash table entries: 1024 (order: 0, 4096 bytes)
[    0.094273] clocksource: jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 19112604462750000 ns
[    0.104000] pinctrl core: initialized pinctrl subsystem
[    0.110376] NET: Registered protocol family 16
[    0.597358] mt7620-pci 10140000.pcie: PCIE0 no card, disable it(RST&CLK)
[    0.603842] mt7620-pci: probe of 10140000.pcie failed with error -1
[    0.623494] rt2880_gpio 10000600.gpio: registering 24 gpios
[    0.628939] rt2880_gpio 10000600.gpio: registering 24 irq handlers
[    0.635083] rt2880_gpio 10000638.gpio: registering 16 gpios
[    0.640476] rt2880_gpio 10000638.gpio: registering 16 irq handlers
[    0.646595] rt2880_gpio 10000660.gpio: registering 32 gpios
[    0.651989] rt2880_gpio 10000660.gpio: registering 32 irq handlers
[    0.658118] rt2880_gpio 10000688.gpio: registering 1 gpios
[    0.663427] rt2880_gpio 10000688.gpio: registering 1 irq handlers
[    0.671035] clocksource: Switched to clocksource systick
[    0.677709] NET: Registered protocol family 2
[    0.682971] TCP established hash table entries: 1024 (order: 0, 4096 bytes)
[    0.689699] TCP bind hash table entries: 1024 (order: 0, 4096 bytes)
[    0.695923] TCP: Hash tables configured (established 1024 bind 1024)
[    0.702150] UDP hash table entries: 256 (order: 0, 4096 bytes)
[    0.707779] UDP-Lite hash table entries: 256 (order: 0, 4096 bytes)
[    0.714191] NET: Registered protocol family 1
[    0.718935] rt-timer 10000100.timer: maximum frequency is 1220Hz
[    0.725788] futex hash table entries: 256 (order: -1, 3072 bytes)
[    0.752303] squashfs: version 4.0 (2009/01/31) Phillip Lougher
[    0.757924] jffs2: version 2.2 (NAND) (SUMMARY) (LZMA) (RTIME) (CMODE_PRIORITY) (c) 2001-2006 Red Hat, Inc.
[    0.770135] io scheduler noop registered
[    0.773962] io scheduler deadline registered (default)
[    0.779155] ralink-usb-phy usbphy: invalid resource
[    0.784554] Serial: 8250/16550 driver, 16 ports, IRQ sharing enabled
[    0.794475] console [ttyS0] disabled
[    0.797918] 10000c00.uartlite: ttyS0 at MMIO 0x10000c00 (irq = 20, base_baud = 2500000) is a Palmchip BK-3103
[    0.807597] console [ttyS0] enabled
[    0.807597] console [ttyS0] enabled
[    0.814637] bootconsole [early0] disabled
[    0.814637] bootconsole [early0] disabled
[    0.829780] spi spi0.0: force spi mode3
[    0.834278] m25p80 spi0.0: w25q128 (16384 Kbytes)
[    0.839120] 5 ofpart partitions found on MTD device spi0.0
[    0.844758] Creating 5 MTD partitions on "spi0.0":
[    0.849655] 0x000000000000-0x000000030000 : "u-boot"
[    0.856557] 0x000000030000-0x000000040000 : "u-boot-env"
[    0.863905] 0x000000040000-0x000000050000 : "factory"
[    0.870848] 0x000000050000-0x000000fd0000 : "firmware"
[    0.960253] 2 uimage-fw partitions found on MTD device firmware
[    0.966337] 0x000000050000-0x000000195cb0 : "kernel"
[    0.973050] 0x000000195cb0-0x000000fd0000 : "rootfs"
[    0.979908] mtd: device 5 (rootfs) set to be root filesystem
[    0.985896] 1 squashfs-split partitions found on MTD device rootfs
[    0.992233] 0x0000003a0000-0x000000fd0000 : "rootfs_data"
[    0.999609] 0x000000ff0000-0x000001000000 : "art"
[    1.007374] rt2880-pinmux pinctrl: pin io40 already requested by pinctrl; cannot claim for 10100000.ethernet
[    1.017481] rt2880-pinmux pinctrl: pin-40 (10100000.ethernet) status -22
[    1.024338] rt2880-pinmux pinctrl: could not request pin 40 (io40) from group ephy  on device rt2880-pinmux
[    1.034292] mtk_soc_eth 10100000.ethernet: Error applying setting, reverse things back
[    1.045113] gsw: setting port4 to ephy mode
[    1.049428] mtk_soc_eth 10100000.ethernet eth0 (uninitialized): port 0 link up (100Mbps/Full duplex)
[    1.058751] mtk_soc_eth 10100000.ethernet eth0 (uninitialized): port 1 link up (100Mbps/Full duplex)
[    1.068296] mtk_soc_eth 10100000.ethernet: loaded mt7620 driver
[    1.075177] mtk_soc_eth 10100000.ethernet eth0: mediatek frame engine at 0xb0100000, irq 5
[    1.084209] rt2880_wdt 10000120.watchdog: Initialized
[    1.090952] NET: Registered protocol family 10
[    1.099257] NET: Registered protocol family 17
[    1.103967] bridge: automatic filtering via arp/ip/ip6tables has been deprecated. Update your scripts to load br_netfilter if you need this.
[    1.116916] Bridge firewalling registered
[    1.121033] 8021q: 802.1Q VLAN Support v1.8
[    1.141263] VFS: Mounted root (squashfs filesystem) readonly on device 31:5.
[    1.149534] Freeing unused kernel memory: 196K (803cf000 - 80400000)
[    3.574397] init: Console is alive
[    3.578095] init: - watchdog -
[    5.306490] usbcore: registered new interface driver usbfs
[    5.312290] usbcore: registered new interface driver hub
[    5.317840] usbcore: registered new device driver usb
[    5.328934] ehci_hcd: USB 2.0 'Enhanced' Host Controller (EHCI) Driver
[    5.337284] ehci-platform: EHCI generic platform driver
[    5.353022] phy phy-usbphy.0: remote usb device wakeup disabled
[    5.359069] phy phy-usbphy.0: UTMI 16bit 30MHz
[    5.363646] ehci-platform 101c0000.ehci: EHCI Host Controller
[    5.369543] ehci-platform 101c0000.ehci: new USB bus registered, assigned bus number 1
[    5.377764] ehci-platform 101c0000.ehci: irq 26, io mem 0x101c0000
[    5.390056] ehci-platform 101c0000.ehci: USB 2.0 started, EHCI 1.00
[    5.397586] hub 1-0:1.0: USB hub found
[    5.401852] hub 1-0:1.0: 1 port detected
[    5.408997] ohci_hcd: USB 1.1 'Open' Host Controller (OHCI) Driver
[    5.416850] ohci-platform: OHCI generic platform driver
[    5.422522] ohci-platform 101c1000.ohci: Generic Platform OHCI controller
[    5.429481] ohci-platform 101c1000.ohci: new USB bus registered, assigned bus number 2
[    5.437689] ohci-platform 101c1000.ohci: irq 26, io mem 0x101c1000
[    5.468344] hub 2-0:1.0: USB hub found
[    5.472526] hub 2-0:1.0: 1 port detected
[    5.484005] init: - preinit -
[    6.443976] 8021q: adding VLAN 0 to HW filter on device eth0
Press the [f] key and hit [enter] to enter failsafe mode
Press the [1], [2], [3] or [4] key and hit [enter] to select the debug level
[    7.945804] jffs2: notice: (362) jffs2_build_xattr_subsystem: complete building xattr subsystem, 0 of xdatum (0 unchecked, 0 orphan) and 0 of xref (0 dead, 0 orphan) found.
[    7.967153] mount_root: switching to jffs2 overlay
[    8.016696] procd: - early -
[    8.019758] procd: - watchdog -
[    8.478127] procd: - ubus -
[    8.506757] random: ubusd urandom read with 18 bits of entropy available
[    8.515380] procd: - init -
Please press Enter to activate this console.
[    9.329799] ip6_tables: (C) 2000-2006 Netfilter Core Team
[    9.349997] Loading modules backported from Linux version wt-2016-05-12-0-g7a54796
[    9.357807] Backport generated by backports.git backports-20160216-0-ge3c56e4
[    9.425678] ip_tables: (C) 2000-2006 Netfilter Core Team
[    9.443155] nf_conntrack version 0.5.0 (1964 buckets, 7856 max)
[    9.500146] xt_time: kernel timezone is -0000
[    9.518257] PPP generic driver version 2.4.2
[    9.526238] NET: Registered protocol family 24
[    9.549584] ieee80211 phy0: rt2x00_set_rt: Info - RT chipset 5390, rev 0500 detected
[    9.557580] ieee80211 phy0: rt2x00_set_rf: Info - RF chipset 7620 detected
[   17.684760] 8021q: adding VLAN 0 to HW filter on device eth0
[   17.699304] device eth0.1 entered promiscuous mode
[   17.704213] device eth0 entered promiscuous mode
[   17.732367] br-lan: port 1(eth0.1) entered forwarding state
[   17.738169] br-lan: port 1(eth0.1) entered forwarding state
[   19.734405] br-lan: port 1(eth0.1) entered forwarding state
[   20.647208] IPv6: ADDRCONF(NETDEV_UP): wlan0: link is not ready
[   20.667245] device wlan0 entered promiscuous mode
[   20.672138] br-lan: port 2(wlan0) entered forwarding state
[   20.677852] br-lan: port 2(wlan0) entered forwarding state
[   20.684568] br-lan: port 2(wlan0) entered disabled state
[   20.750971] IPv6: ADDRCONF(NETDEV_CHANGE): wlan0: link becomes ready
[   20.757731] br-lan: port 2(wlan0) entered forwarding state
[   20.763391] br-lan: port 2(wlan0) entered forwarding state
[   22.754391] br-lan: port 2(wlan0) entered forwarding state
[   37.605653] random: nonblocking pool is initialized


BusyBox v1.24.2 () built-in shell (ash)

  _______                     ________        __
 |       |.-----.-----.-----.|  |  |  |.----.|  |_
 |   -   ||  _  |  -__|     ||  |  |  ||   _||   _|
 |_______||   __|_____|__|__||________||__|  |____|
          |__| W I R E L E S S   F R E E D O M
 -----------------------------------------------------
 DESIGNATED DRIVER (Bleeding Edge, 50013)
 -----------------------------------------------------
  * 2 oz. Orange Juice         Combine all juices in a
  * 2 oz. Pineapple Juice      tall glass filled with
  * 2 oz. Grapefruit Juice     ice, stir well.
  * 2 oz. Cranberry Juice
 -----------------------------------------------------
root@OpenWrt:/# 
```

To PgUp and PgDn in screen you need to enter copy mode using Ctrl-A and then
press [, you can then go up and down by half a page at a time using Ctrl-u and
Ctrl-d.

To enable a non-root user to access the tty device create the file
`/etc/udev/rules.d/60-ttyUSBx.rules` with this content:

```
KERNEL=="ttyUSB[0-9]",              MODE="0666"
```

