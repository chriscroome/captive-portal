#!/usr/bin/env bash

# This is a script to copy some data onto a GL-MT300A router
#
# Copyright (c) 2016 Chris Croome
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details. 
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# The IP addess of the device, by default this is 192.168.1.1
IP="192.168.1.1"

# The destination for the files on the router
DEST="/www/html"

# Check which is available 
WHICH=$(which which)
if [[ -z "${WHICH}" ]]; then
  echo "Sorry which couldn't be found, please install it"
  exit 1
fi

# Check ssh and scp are available 
SSHCHECK=$(${WHICH} ssh)
SCPCHECK=$(${WHICH} scp)
if [[ -e "${SSHCHECK}" && -e "${SCPCHECK}" ]]; then
  # The SSH command, using the default user and not checking the host key
  SSH="${SSHCHECK} -q -oStrictHostKeyChecking=no root@${IP}"
  # The SCP command, not checking the host key
  SCP="${SCPCHECK} -q -oStrictHostKeyChecking=no"
else
  echo "This script depends on ssh and scp, please install them"
  exit 1
fi

# The current working directory 
PWD=$(${WHICH} pwd)
if [[ -z "${PWD}" ]]; then
  echo "Sorry pwd couldn't be found, please install it"
  exit 1
else
  DIR=$(pwd)
fi

# The source of the files to upload
SOURCE="${DIR}/html"
# Make the directory if it doesn't exist
if [[ ! -d "${SOURCE}" ]]; then
  mkdir "${SOURCE}" && echo "${SOURCE} didn't exist so it has been created, add your HTML files here" \
    || { echo "There was a problem creating ${SOURCE}" ; exit 1 ; }
fi 

# Check we have rsync installed locally
LOCALRSYNC=$(${WHICH} rsync)
if [[ -f "${LOCALRSYNC}" ]]; then
  # Check we have rsync installed on the router
  REMOTERSYNC=$(${SSH} "which rsync")
  if [[ "${REMOTERSYNC}" != /usr/bin/rsync ]]; then 
    # Update the package list
    echo "Updating the package list..."
    ${SSH} "opkg -V0 update" && echo "Package list updated" \
      || { echo "There was a problem updating the package list on the router" ; exit 1 ; }
    # Install rsync
    ${SSH} "opkg -V0 install rsync" && echo "rsync installed on the router" \
      || { echo "There was a problem installing rsync on the router"; exit 1 ; }
  fi
  # Check we have some content to upload
  if [[ ! -f "${SOURCE}/index.html" ]]; then
    echo "No content found in ${SOURCE} so creating a basic HTML file to upload"
    echo "<h1>Hello World!</h1>" > "${SOURCE}/index.html"
  fi  
  # We have rsync locally and remotly so sync the files
  echo "Starting rsync from ${SOURCE}/ to root@${IP}:${DEST}/"
  ${LOCALRSYNC} -aq ${SOURCE}/ root@${IP}:${DEST}/ \
    && echo "Files rsynced from ${SOURCE}/ to root@${IP}:${DEST}/" \
    || { echo "Problem rsyncing from ${SOURCE}/ to root@${IP}:${DEST}/" ; exit 1 ; }
else
  echo "Please install rsync"
  exit
fi
  
exit 0
