#!/usr/bin/env bash

# This is a script to set up ssh and WIFI on a GL-MT300A router
#
# Copyright (c) 2016 Chris Croome
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# The IP address of the device, by default this is 192.168.1.1
IP="192.168.1.1"

# Check which is available 
WHICH=$(which which)
if [[ -z "${WHICH}" ]]; then
  echo "Sorry which couldn't be found, please install it"
  exit 1
fi

# Check ssh and scp are available 
SSHCHECK=$(${WHICH} ssh)
SCPCHECK=$(${WHICH} scp)
if [[ -e "${SSHCHECK}" && -e "${SCPCHECK}" ]]; then
  # The SSH command, using the default user and not checking the host key
  SSH="${SSHCHECK} -q -oStrictHostKeyChecking=no root@${IP}"
  # The SCP command, not checking the host key
  SCP="${SCPCHECK} -q -oStrictHostKeyChecking=no"
else
  echo "This script depends on ssh and scp, please install them"
  exit 1
fi

# The current working directory 
PWD=$(${WHICH} pwd)
if [[ -z "${PWD}" ]]; then
  echo "Sorry pwd couldn't be found, please install it"
  exit 1
else
  DIR=$(pwd)
fi

# If we have just flashed a new image the server key will have changed 
# so delete the old one
SSHKEYGEN=$(${WHICH} ssh-keygen)
if [[ -z "${SSHKEYGEN}" ]]; then
  echo "Sorry ssh-keygen couldn't be found, please install it" 
  exit 1
else
  if [[ -e "${SSHKEYGEN}" ]]; then
    # Check the known_hosts files exists
    if [[ -f "${HOME}/.ssh/known_hosts" ]]; then
      ${SSHKEYGEN} -q -f "${HOME}/.ssh/known_hosts" -R ${IP} \
        && echo "${IP} removed from ${HOME}/.ssh/known_hosts" \
        || { echo "Failed to remove ${IP} from ~/.ssh/known_hosts" ; exit 1 ; } 
    else
      echo "The ${HOME}/.ssh/known_hosts file doesn't exit so ${IP} can't been removed from it"
    fi
  fi
fi

# Check if we have a authorized_keys file in the current directory and 
# upload it if we do
if [[ -f "${DIR}/authorized_keys" ]]; then
  ${SCP} ${DIR}/authorized_keys root@${IP}:/etc/dropbear/ \
    && echo "${DIR}/authorized_keys uploaded to the router" \
    || { echo "Failed to upload authorized_keys to the router" ; exit 1 ; }
else
  echo "A SSH authorized_keys file wasn't found in ${DIR}"
  # Check if any public keys exist
  PUBKEYS=$(ls ${HOME}/.ssh/*.pub) 
  if [[ -z "${PUBKEYS}" ]]; then
    echo "You can generate a ssh key pair by running the following command:"
    echo "${SSHKEYGEN} -t rsa -b 4096"
  fi 
  echo "You can copy your public key(s) for uploading to the router by" 
  echo "running the following command:" 
  echo "cat ${HOME}/.ssh/*.pub > ${DIR}/authorized_keys"
  echo "If you don't use key based authentication you will need to type or"
  echo "paste the root password a lot so it is strongly suggested that you"
  echo "set up ssh keys"
fi

# Check if the password for the router is set in $OPENWRTPASSWD and generate
# a random one if it isn't
if [[ -z "${OPENWRTPASSWD}" ]]; then
  echo "The OPENWRTPASSWD environmental variable isn't set, so generating a random password" 
  echo "To set a password to be used for the router run the following before you run this script:"
  echo "export OPENWRTPASSWD=yoursecurepassword"
  # Check if pwgen is installed
  PWGENGEN=$(${WHICH} pwgen)
  # If pwgen can't be found 
  if [[ -z "${PWGENGEN}" ]]; then
    # Check if we have md5sum
    MD5SUM=$(${WHICH} md5sum)
    # If we don't have md5sum check if we have md5 (OSX)
    if [[ -z "${MD5SUM}" ]]; then
      MD5SUM=$(${WHICH} md5)
    fi
    # If we have something to generate an md5sum
    if [[ -e "${MD5SUM}" ]]; then
      DATE=$(${WHICH} date)
      CUT=$(${WHICH} cut)
      # Check we have date and cut
      if [[ -e "${DATE}" && -e "${CUT}" ]]; then
        # Generate a password using the first 12 characters of the md5sum of the date 
        OPENWRTPASSWD=$(${DATE} | ${MD5SUM} | ${CUT} -c1-12)
      fi
    fi
  else
    # Generate a password using pwgen
    OPENWRTPASSWD=$(${PWGENGEN} -n 14 1)
  fi
  # Check we have ended up with a password
  if [[ -z "${OPENWRTPASSWD}" ]]; then
    echo "Sorry generating a random password failed"
    OPENWRTPASSWD="password123"
  fi
  echo "The password that is to be used for the device is ${OPENWRTPASSWD}"
  echo "${OPENWRTPASSWD}" >> ${DIR}/passwd.txt
  echo "This password has been appended to ${DIR}/passwd.txt" 
fi

# Set the root password on the router to $OPENWRTPASSWD
${SSH} "echo -e '${OPENWRTPASSWD}\n${OPENWRTPASSWD}' | passwd" \
  && echo "Router password set" \
  || { echo "Failed to set the password on the router, please wait a moment and run $0 again." ; exit 1 ; }

# Enable wifi on the router
${SSH} "uci set wireless.@wifi-device[0].disabled=0" \
  && echo "WIFI enabled via /etc/config/wireless" \
  || { echo "Failed to enable WIFI on the router" ; exit 1 ; }
${SSH} "uci commit wireless" \
  && echo "WIFI changes in /etc/config/wireless committed" \
  || { echo "Failed commit changes to /etc/config/wireless" ; exit 1 ; }
${SSH} "wifi" \
  && echo "WIFI started the SSID is OpenWRT" \
  || { echo "Failed to start WIFI" ; exit 1 ; }
#echo "The router is now going to be rebooted..."
#${SSH} "reboot" \
#  && { echo "Rebooting the router, please wait a couple of minutes" ; sleep 120 ; } \
#  || { echo "Failed to reboot the router" ; exit 1 ; }

exit 0
